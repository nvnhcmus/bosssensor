import cv2
import sys



if __name__ == '__main__':
    if(len(sys.argv) != 3):
        print("invalid agrs")
        sys.exit(0)
    number = int (sys.argv[1])
    path = sys.argv[2]

    cap = cv2.VideoCapture(0)
    cascade_path = "haarcascade_frontalface_default.xml"
    counter = 0
    while True:
        _, frame = cap.read()
        img_name = 'data/' + path + '/mg_CV4_%s.jpg' % counter;
        cv2.imwrite(img_name, frame, [int(cv2.IMWRITE_JPEG_QUALITY), 90])
        counter = counter + 1
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        cv2.imshow('image_', frame)
        k = cv2.waitKey(10)
        if k == 27:
            break
        if counter >= number:
            break
    cap.release()
    cv2.destroyAllWindows()
    print ("program exited successfuly")
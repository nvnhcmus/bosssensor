# -*- coding:utf-8 -*-
import cv2

if __name__ == '__main__':
    cap = cv2.VideoCapture(0)
    cascade_path = "haarcascade_frontalface_default.xml"
    counter = 0
    while True:
        _, frame = cap.read()
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # cv2.imshow('image_', frame)
        cascade = cv2.CascadeClassifier(cascade_path)
        facerect = cascade.detectMultiScale(frame_gray, scaleFactor=1.2, minNeighbors=3, minSize=(10, 10))
        print( len(facerect))
        if len(facerect) > 0:
            print('face detected')
            color = (160, 158,95)  # 白
            for rect in facerect:
                # 検出した顔を囲む矩形の作成
                cv2.rectangle(frame, tuple(rect[0:2]), tuple(rect[0:2] + rect[2:4]), color, thickness=2)
                cv2.imshow('image_', frame)
                x, y = rect[0:2]
                width, height = rect[2:4]
                image = frame[y - 10: y + height, x: x + width]
                print("x: ", x, " y: ", y, " width: ", width, " height: ", height)
        k = cv2.waitKey(10)
        #Escキーを押されたら終了
        if k == 27:
            break

    #キャプチャを終了
    cap.release()
    cv2.destroyAllWindows()

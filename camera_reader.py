# -*- coding:utf-8 -*-
import cv2

from boss_train import Model
from image_show import show_image


if __name__ == '__main__':
    cap = cv2.VideoCapture(0)
    cascade_path = "haarcascade_frontalface_default.xml"
    model = Model()
    model.load()
    counter = 0
    while True:
        _, frame = cap.read()
        #cv2.imshow('image_origin', frame)
        # Load an color image
        #frame = cv2.imread('D:\BossSensor\data\boss\images.jpg', 1)
        #img_name = 'data/mg_CV2_%s.jpg' % counter;
        #counter = counter + 1
        #cv2.imwrite(img_name, frame, [int(cv2.IMWRITE_JPEG_QUALITY), 90])

        # グレースケール変換
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        # カスケード分類器の特徴量を取得する
        cascade = cv2.CascadeClassifier(cascade_path)

        # 物体認識（顔認識）の実行
        facerect = cascade.detectMultiScale(frame_gray, scaleFactor=1.2, minNeighbors=3, minSize=(10, 10))
        #facerect = cascade.detectMultiScale(frame_gray, scaleFactor=1.01, minNeighbors=3, minSize=(3, 3))
        if len(facerect) > 0:
            print('face detected')
           
            color = (255, 255, 255)  # 白
            for rect in facerect:
                # 検出した顔を囲む矩形の作成
                cv2.rectangle(frame, tuple(rect[0:2]), tuple(rect[0:2] + rect[2:4]), color, thickness=2)

                x, y = rect[0:2]
                width, height = rect[2:4]
                image = frame[y - 10: y + height, x: x + width]

                result = model.predict(image)
                #cv2.imshow('image_ret',image)
                print ('result is %d',  result)
                if result == 0:  # boss
                    print('Boss is approaching')
                    cv2.imshow('image_result', frame)
                    #show_image()
                else:
                    print('Not boss')

        #10msecキー入力待ち
        k = cv2.waitKey(10)
        #Escキーを押されたら終了
        if k == 27:
            break

    #キャプチャを終了
    cap.release()
    cv2.destroyAllWindows()

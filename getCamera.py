from image_show import show_image
import cv2

if __name__ == '__main__':
    cap = cv2.VideoCapture(0)
    cascade_path = "haarcascade_frontalface_default.xml"
    counter = 0
    while True:
        _, frame = cap.read()
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        cv2.imshow('image_', frame)
        cascade = cv2.CascadeClassifier(cascade_path)
        facerect = cascade.detectMultiScale(frame_gray, scaleFactor=1.2, minNeighbors=3, minSize=(10, 10))
        if len(facerect) > 0:
            print('face detected')
            color = (255, 255, 255)
            img_name = 'data/boss/mg_CV4_%s.jpg' % counter;
            cv2.imwrite(img_name, frame, [int(cv2.IMWRITE_JPEG_QUALITY), 90])
            counter = counter + 1
        k = cv2.waitKey(10)
        if k == 27:
            break
    cap.release()
    cv2.destroyAllWindows()
